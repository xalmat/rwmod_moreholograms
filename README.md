Adds a greater variety of holograms to Save Our Ship 2, and allows holographic avatars to be unlocked much earlier in the game.
	
[h1]Features[/h1]

[list]
[*] Unlocks the ability to project holographic avatars much earlier
[list]
[*] Crashlanded and SoS2 starting scenarios now have holographic avatars unlocked by default.
[*] Make afterlife caskets available much earlier in the game as a result. They still require cryptosleep casket research
[/list]
[*] More hologram types are now available.
[list]
[*]Basic holograms - perfect for low skilled tasks, do not require rest, cannot learn, cannot form social bonds
[*]Sentient holograms - Can learn like a baseline human but cannot form social bonds
[*]Sapient holograms - Superior stats to a human for skilled labor, can form social bonds
[*]Other types to be discovered, and more to come[/list]
[*] Craft your own artificial brains, including your own persona cores.
[list][*]Tired of your humans possessing feeble, organic brains? Replace them with machine brains intead
[*]Vanilla [i]AI Persona Cores[/i] have been renamed to [i]machine brain - sapient[/i], and have been made substantially more valuable than before.
[/list]
[*]Start in space as a hologram-only crew with exclusive starter ships and starting scenarios!
[*]Wear holographic clothes! Reinforce your clothes with holo-emitters to change their appearance or make them stronger!
[*]Craft your own holographic prosthetics!
[/list]

[h1]Hologram types[/h1]

No longer confined to ship computer cores, you can now build free-standing holographic emitters that house an artificial mind.

[list]
[*][b][u]Basic[/u][/b] - Do not require rest, but cannot learn and cannot form social bonds, very low chance of mood break
[*][b][u]Sentient[/u][/b] - Can slowly learn, cannot form social bonds. Low chance of mood break
[*][b][u]Specialist[/u][/b] - Do not require rest, specialized in certain skills at the expense of others.
[*][b][u]Sapient[/u][/b] - Equivalent to a human mind.
[*][b][u]Archotech[/u][/b] - Perfection, but extremely rare and hard to find.
[/list]

[h1]Hologram Upgrades[/h1]

After the appropriate research, you can craft hologram upgrade modules. These modular upgrades are multi-purpose, so no need to craft many different modules for different roles. They are applied by performing [i]surgery[/i] on the hologram using these upgrade modules as well as a component. These require a high Intellectual skill to perform successfully. The list of upgrades includes, but isn't limited to:

[list]
[*]Better defense
[*]Better manipulation
[*]Less need for sleep
[*]Better lovin'
[/list]

And many more to be discovered and added!

[h1]Crafting an artificial brain[/h1]

In order to craft your own artificial brain, you first need to create the various persona cores for the brain. Persona cores are meant to be very complex crafting projects, broken up into smaller segments. Craft a blank persona core, then reprogram it to the desired function.

Due to the complex nature of these smaller cores, and due to the limited resources found on the Rim, mass production of these modules is not possible.

Creating a fully functional artificial brain --- one that mimics human brains --- will require a wide variety of skills beyond just crafting. It is very unlikely a single person will possess the skills necessary to complete the job. It might take several quadrums of work to make a single brain.

If you are lucky, you might be able to purchase pre-programmed modules from traders...for a steep price, of course.

A subpersona brain is suitable for a basic hologram, while sentient and sapient brains are suitable for their respective holograms. Other hologram types have different requirements.

[h1]Surrogate brain surgery[/h1]

Tired of your humans having feeble, flawed, organic brains? Replace them with machine brains! Every artificial brain available can be installed in a human to replace their organic one.

Surrogate brains will have similar capacities as their hologram equivalents. Only a Sapient brain can truly contain the human mind; anything less will result in someone becoming [i]less[/i] than human.

Brain surgery is [i]extremely[/i] risky surgery, and will require a large amount of Glitterworld medicine to even perform. Failure has an extremely high chance of killing the patient (if they somehow survive a failed surgery, they will almost certainly have brain damage). Only perform brain surgery in a clean hospital with proper medical facilities, and only let your best surgeon perform this surgery.

[h1]Holographic clothes[/h1]

Fitting with the theme of more holograms, now your clothes can be holographic too! They come in three varieties. In all cases, those with ideological nudist beliefs love these clothes, and will happily wear them (normal nudists are still not happy to wear them):

[list][*][b]Holographic[/b] - Appearance only clothes, very easy to make, very beautiful, but provides no protection whatsoever.
[*][b]Transparent[/b] - regular clothes embedded with holo-emitters that render them transparent when worn.
[*][b]Hardlight[/b] - Advanced reinforced clothing. These are better than normal clothes in every way![/list]

[h1]Holographic prosthetics[/h1]

Lose a body part, and want something different than normal bionics? Enter holographic prosthetics. Craft holographic implants that substitute for a normal organic body part, and are modular by design, meaning the same implant can be used for multiple body parts. EMPs may cause strange side effects however.

[h1]Issues[/h1]

On occasion a hologram may spawn with some missing skills. Unfortunately this is a bug in SoS2 and not one that I can resolve myself.

[h1]Compatibility[/h1]

This mod requires Rimworld version 1.3. I have no plans to backport this mod to previous Rimworld versions.

This mod should be compatible with everything that Save Our Ship 2 is compatible with, but I haven't tested. It needs Save Our Ship 2 to work.

This mod should be safe to use on an existing save. If you haven't researched holographic avatars yet, you will need to research it.

Any mod that modifies the vanilla AI Persona Core, including mods that add a recipe for it, will conflict with this mod. Whichever mod is loaded later in the mod load order will take priority.

This mod is not intended to be compatible with Better Holograms. However there are mechanisms in this mod to make Better Holograms redundant.

[h1]Contribute![/h1]

You can contribute at my gitlab:

https://gitlab.com/xalmat/rwmod_moreholograms

[h1]Credit[/h1]

Thanks to Thain and Kentington for SoS2.

Thank you to Proxyer, Hypertripp, CrazyOatmeal, and everyone else I missed for their bug reports.

Thank you (Insert Boi here) for the starter ship design!

Holography Station artwork by CrazyOatmeal.

Thanks to the SOS2 Discord and everyone else for their contributions.

